package com.paytouch.developer.api.rest;

import org.springframework.stereotype.Controller;

import com.paytouch.developer.api.PhoneApi;
import com.paytouch.developer.domain.Phone;

/**
 * This class will listen for incoming connections related to phones
 * The entry point must be /rest/phone
 * @author developer
 *
 */
@Controller
public class PhoneApiRest implements PhoneApi {

	@Override
	public void createPhone(Phone phone) {
		// TODO Auto-generated method stub

	}

	@Override
	public Phone retrievePhone(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updatePhone(Integer id, Phone phone) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deletePhone(Integer id) {
		// TODO Auto-generated method stub

	}

	@Override
	public Phone getMostSoldPhone() {
		// TODO Auto-generated method stub
		return null;
	}

}
