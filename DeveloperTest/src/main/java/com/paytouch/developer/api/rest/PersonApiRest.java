package com.paytouch.developer.api.rest;

import java.util.List;

import org.springframework.stereotype.Controller;

import com.paytouch.developer.api.PersonApi;
import com.paytouch.developer.domain.Person;

/**
 * This class will listen for incoming connections related to persons
 * The entry point must be /rest/person
 * @author developer
 *
 */
@Controller
public class PersonApiRest implements PersonApi {

	@Override
	public List<Person> getPersonsWithMoreThanOnePhone() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Person getSpendthriftPerson() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void create(Person person) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Person retrievePerson(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Integer id, Person person) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deletePerson(Integer id) {
		// TODO Auto-generated method stub
		
	}

}
