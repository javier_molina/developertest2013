package com.paytouch.developer.api;

import com.paytouch.developer.domain.Phone;

public interface PhoneApi {

	/**
	 * Creates a phone
	 * @param phone
	 */
	public void createPhone(Phone phone);

	/**
	 * Retrieve a phone
	 * @param id
	 * @param phone
	 */
	public Phone retrievePhone(Integer id);
	
	/**
	 * Updates a phone
	 * @param id
	 * @param phone
	 */
	public void updatePhone(Integer id, Phone phone);
	
	/**
	 * Delete a phone
	 * @param id
	 * @param phone
	 */
	public void deletePhone(Integer id);
	
	/**
	 * Retrieve the most sold phone
	 * @return the most sold phone
	 */
	public Phone getMostSoldPhone();
}
