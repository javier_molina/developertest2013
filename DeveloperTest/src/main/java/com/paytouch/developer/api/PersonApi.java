package com.paytouch.developer.api;

import java.util.List;

import com.paytouch.developer.domain.Person;

public interface PersonApi {

	/**
	 * Retrieves all persons who have more than one phone
	 * @return The persons who have more than one phone
	 */
	public List<Person> getPersonsWithMoreThanOnePhone();
	
	/**
	 * Retrieve the person who spent more money on phones.
	 * If two persons spent the same amount, any of them is returned
	 * @return The person who spent more money on phones
	 */
	public Person getSpendthriftPerson();
	
	/**
	 * Create a new person
	 * @param person
	 */
	public void create(Person person);
	
	/**
	 * Retrieve a person
	 * @param person
	 */
	public Person retrievePerson(Integer id);
	
	/**
	 * Update a person
	 * @param person
	 */
	public void update(Integer id, Person person);
	
	/**
	 * Remove the person identified by id
	 * @param id
	 */
	public void deletePerson(Integer id);
	
	
}
