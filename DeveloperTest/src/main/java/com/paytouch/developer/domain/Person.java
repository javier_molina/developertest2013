package com.paytouch.developer.domain;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "persons")
public class Person {

	public static String _NAME 		= "name";
	public static String _AGE 		= "age";
	public static String _PHONES	= "phones";
	@Id
	private Integer id;
	private String name;
	private Integer age;
	private List<Phone> phones;
	
	public Person(Integer id, String name, Integer age, List<Phone> phones) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.phones = phones;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public List<Phone> getPhones() {
		return phones;
	}
	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}
}
