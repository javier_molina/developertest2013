package com.paytouch.developer.dao;

import com.paytouch.developer.domain.Phone;

public interface PhoneDao extends GenericDao<Phone> {

	/**
	 * Retrieves the most sold phone
	 * @return The phone which more people has
	 */
	public Phone getMostSoldPhone();
}
