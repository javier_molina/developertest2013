package com.paytouch.developer.dao;

import java.util.List;

import com.paytouch.developer.domain.Person;

public interface PersonDao extends GenericDao<Person> {

	/**
	 * Retrieves all persons who have more than one phone
	 * @return The persons who have more than one phone
	 */
	public List<Person> getPersonsWithMoreThanOnePhone();
	
	/**
	 * Retrieve the person who spent more money on phones.
	 * If two persons spent the same amount, any of them is returned
	 * @return The person who spent more money on phones
	 */
	public Person getSpendthriftPerson();
}
