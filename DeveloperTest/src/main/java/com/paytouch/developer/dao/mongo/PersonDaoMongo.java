package com.paytouch.developer.dao.mongo;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.paytouch.developer.dao.PersonDao;
import com.paytouch.developer.domain.Person;

@Repository
public class PersonDaoMongo implements PersonDao {

	@Autowired MongoTemplate mongo;

	@Override
	public void save(Person person){
		// TODO Auto-generated method stub
	}
	
	@Override
	public Collection<Person> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Person> getPersonsWithMoreThanOnePhone() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Person getSpendthriftPerson() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Person findById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

}
