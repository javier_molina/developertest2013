package com.paytouch.developer.dao;

import java.util.Collection;

public interface GenericDao<T> {

	/**
	 * Save or update an entity
	 * @param entity
	 */
	public void save(T entity);
	
	/**
	 * @return The entire collection
	 */
	public Collection<T> findAll();
	
	/**
	 * @return The object identified by id
	 */
	public T findById(Integer id);
}
