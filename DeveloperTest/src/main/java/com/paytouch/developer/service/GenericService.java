package com.paytouch.developer.service;

public interface GenericService<T> {

	public void save(T entity);
	
	public void delete(Integer identifier);
	
	public T read(Integer identifier);
}
