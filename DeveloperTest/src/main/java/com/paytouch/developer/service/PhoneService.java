package com.paytouch.developer.service;

import com.paytouch.developer.domain.Phone;

public interface PhoneService extends GenericService<Phone>{

	/**
	 * Creates a phone
	 * @param phone
	 */
	public void createPhone(Phone phone);
	
	/**
	 * Updates a phone
	 * @param id
	 * @param phone
	 */
	public void updatePhone(Integer id, Phone phone);
	
	/**
	 * Delete a phone
	 * @param id
	 * @param phone
	 */
	public void deletePhone(Integer id);
	
	/**
	 * Retrieve a phone
	 * @param id
	 * @param phone
	 */
	public Phone retrievePhone(Integer id);
	
	/**
	 * Retrieve the most sold phone
	 * @return the most sold phone
	 */
	public Phone getMostSoldPhone();
}
