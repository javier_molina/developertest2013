package com.paytouch.developer;

import java.util.ArrayList;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.paytouch.developer.domain.Person;
import com.paytouch.developer.domain.Phone;
import com.paytouch.developer.mongo.config.SpringMongoConfig;

/**
 * Hello world!
 * 
 */
public class App {
	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
		
		mongoOperation.save(new Person(2, "Pepe", 10, new ArrayList<Phone>()));
		
		Query q = new Query(new Criteria("name").is("Jonas"));
		
		Person p = mongoOperation.findOne(q, Person.class);
		
		System.out.println(p.getName() + " , " + p.getAge());
	}
}
