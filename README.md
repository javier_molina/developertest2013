# The README

El objetivo de el test es crear una aplicación capaz de comunicarse con el exterior mediante una API Rest utilizando el framework Spring y MongoDB como base de datos.

La aplicación permitirá:

    Operaciones CRUD sobre personas
    Operaciones CRUD sobre telefonos
    Realizar las siguientes consultas

        Obtener las personas que tienen más de un teléfono

        Obtener la persona que más dinero ha gastado en el teléfono

        Obtener el teléfono más vendido

Es importante que todas las acciones utilicen el protocolo adecuado para su uso

Se valorará:

    La calidad del código

    El resultado obtenido según la herramienta SONAR

    El throughput de la aplicación

    La correcta utilización de las anotaciones

    El correcto funcionamiento de la aplicación

Notas importantes:
- Se puede modificar cualquier cosa perteneciente a la implementación. Las interfícies no pueden modificarse.
- Los ficheros de configuración son inamovibles
- No existe tratamiento de excepciones definido, siéntase libre de definir y implementar un sistema de excepciones. 

